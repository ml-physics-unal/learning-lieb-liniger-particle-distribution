using PyPlot
# using mlpack
import StatsBase
import Distributions
using LinearAlgebra
# using GaussianMixtures
using NPZ

function fit_data_mlpack( X; n_gaussians = 2 )
    g = mlpack.gmm_train( n_gaussians, X; max_iterations=50, kmeans_max_iterations=1000, verbose=true )
    sampled_data = mlpack.gmm_generate( g, 1_000_000 )
    sampled_data
end

function fit_data_gm(X; n_gaussians = 2 )
    g = GMM( n_gaussians, X, kind =:full, nIter = 100 )
    m = Distributions.MixtureModel(g)
    
    y = []
    for i in 1:100_000
        push!( y, rand(m) )
    end

    sampled_data = Array{Float64}(undef, 100_000, 2 )
    for i in 1:100_000
        sampled_data[i,1] = y[i][1]
        sampled_data[i,2] = y[i][2]
    end
    sampled_data
end

function Slater_matrix( xs::Vector{Float64}, ks::Vector{Float64} )
    @assert length(ks) == length(xs)
    n = length(ks)
    S = zeros( ComplexF64, n, n )
   
   for i in 1:n
        for j in 1:n
            @inbounds S[i,j] = exp( im*ks[j]*xs[i] )
        end
    end
    S
end

function psi( xs::Vector{Float64}, ks::Vector{Float64} )
    S = Slater_matrix( xs, ks )
    det(S)
end

function metropolis_hastings_sampling!( Xw, ks::Vector{Float64}, data::Dict{String,Any} )
    n_walkers = data["N_particles"]
    n_steps = data["N_steps"]
    len_fixed_pos = length(data["fixed_index"])
    rng = Distributions.Uniform( data["spacial_boundaries"]... )

    Xw[:,1] = vcat([ rand(rng) for i in 1:n_walkers ])
    
    for k in 1:n_steps-1
    	Xp = [ rand(rng) for i in 1:n_walkers ]

    	for i in 1:len_fixed_pos
    		id = data["fixed_index"][i]
    		val = data["fixed_values"][i]
    		Xp[id] = val
    		Xw[id, k] = val
    	end

    	alpha = abs(psi(Xp, ks))^2/abs(psi(Xw[:,k], ks))^2
    	if alpha >= 1.0
    		@inbounds Xw[:,k+1] = Xp[:]
    	else
    		beta = rand()
    		if beta <= alpha
    			@inbounds Xw[:,k+1] = Xp[:]
    		else
    			@inbounds Xw[:,k+1] = Xw[:,k]
    		end
    	end
    end
end

function compute( ;N = 4, metropolis_steps = 1_000_000 )
    L = 1.0
    ks = Float64[ 2pi/L*( i + (N-1)/2.0 ) for i in 1:N ]
    
    data_sampling = Dict( 
		"N_particles" => N,
		"N_steps" => metropolis_steps,
		"fixed_index" => [],
		"fixed_values" => [],
        "spacial_boundaries" => [ 0, L ]
    )
    
    Xw = zeros( Float64, N, metropolis_steps )
    metropolis_hastings_sampling!( Xw, ks, data_sampling )
    npzwrite("particle_positions_N=$(N)_metro_steps=$(metropolis_steps).npz", Xw)
    
    # figure()
    # hist = StatsBase.fit( StatsBase.Histogram,( Xw[1,:], Xw[2,:] ), nbins=130 )
    # imshow( hist.weights, origin="lower" )
    # colorbar()
    
    # figure()
    # X = hcat( Xw[1,:], Xw[2,:] )
    # Sampled_data = fit_data_mlpack( X; n_gaussians=20 )
    # hist2 = StatsBase.fit( StatsBase.Histogram,(Sampled_data[:,1],Sampled_data[:,2]), nbins=100 )
    # imshow( hist2.weights, origin="lower" )
end

for i in 2:4
    compute( N = i, metropolis_steps = 10_000_000 )
end
