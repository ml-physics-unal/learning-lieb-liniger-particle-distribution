from sklearn.datasets import make_blobs
from sklearn.mixture import GaussianMixture
import os
import itertools
from memory_profiler import memory_usage
import matplotlib.pyplot as plt
import numpy as np
import pickle

try:
    os.mkdir("figures-scaling")
except:
    print("folder 'figures-scaling' already exists.")

n_samples = [1000, 2000, 4000, 8000]
n_particles = [2, 3, 4]
n_gaussians = [2, 4, 8, 16, 32, 64, 128]
mem_usage = []

for nsamp, npart, ngauss in itertools.product(n_samples, n_particles, n_gaussians):
    X, y = make_blobs(n_samples=nsamp, centers=ngauss, n_features=npart, random_state=0)
    gmm = GaussianMixture(n_components=ngauss, covariance_type='full', max_iter=5000, random_state=0)
    musage = memory_usage((gmm.fit, (X,),), interval=.2)
    times = np.arange(start=0, stop=len(musage)*.2-0.01, step=.2)
    print("{0:.1f} seconds and a peak of {4:.1f} MB with {1} samples, {2} particles and {3} gaussians".format(times[-1], nsamp, npart, ngauss, max(musage)))
    fig, ax = plt.subplots()
    ax.plot(times, musage, 'o-')
    ax.set_xlabel('Seconds')
    ax.set_ylabel('MBs')
    ax.set_title("{0} samples, {1} particles and {2} gaussians".format(nsamp, npart, ngauss))
    plt.savefig("figures-scaling/{0} samples, {1} particles and {2} gaussians".format(nsamp, npart, ngauss).replace(" ", "_"))
    plt.close(fig)
    mem_usage.append(musage)

with open("test_GMM_results.p", "wb") as f:
    pickle.dump(mem_usage, f)