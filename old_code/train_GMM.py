from sklearn.mixture import GaussianMixture
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from multiprocessing import Pool
import tqdm
import itertools
from functools import partial
import pickle
import math
from scipy.integrate import nquad


def slater(x, k):
    n = len(k)
    xx, kk = np.meshgrid(x, k)
    S = np.exp(1j * xx * kk)
    return S.T


def get_k(N, L):
    return np.array([2 * np.pi / L * (i + (N - 1) / 2) for i in range(1, N + 1)])


def psi(x, k):
    S = slater(x, k)
    return 1 / np.sqrt(math.factorial(x.size)) * np.linalg.det(S)


def euclidean(x, y):
    return x - y


def gaussian(x, mu, sigma, dfun, K):
    distvec = dfun(x, mu)
    return (
        1
        / np.sqrt((2 * np.pi) ** K * np.linalg.norm(sigma))
        * np.exp(-0.5 * np.matmul(np.matmul(distvec.T, np.linalg.pinv(sigma)), distvec))
    )


def gaussians(x, mus, sigmas, dfun, K, weights):
    result = 0
    for w, mu, sigma in zip(weights, mus, sigmas):
        result += w * gaussian(x, mu, sigma, dfun, K)
    return result


def gaussians_integrate(*args):
    xp = np.array(args[:-4])
    weights = args[-1]
    K = args[-2]
    sigmas = args[-3]
    mus = args[-4]
    return gaussians(xp, mus, sigmas, euclidean, K, weights)


def wavefunction_integrate(*args):
    xp = np.array(args[:-1])
    k = args[-1]
    return np.abs(psi(xp, k)) ** 2


def overlap_integrate(*args):
    xp = np.array(args[:-6])
    weights = args[-1]
    K = args[-2]
    sigmas = args[-3]
    mus = args[-4]
    k = args[-5]
    factor = args[-6]
    return np.abs(psi(xp, k)) * np.sqrt(
        factor * gaussians(xp, mus, sigmas, euclidean, K, weights)
    )


def get_points_in_box(data, L):
    conditions = []
    for i in range(data.shape[1]):
        conditions.append(data[:, i] >= 0)
        conditions.append(data[:, i] <= L)
    idx = np.where(np.all(conditions, axis=0))
    return data[idx]


def fit_GMM(filename, n_components, sample_size, L=1, verbose=False):
    """
    This function fits a GMM to the Lieb-Lineger data obtained through MCMC
    Args:
        filename string: path to the...
    """

    data = np.load(
        filename
    ).T  # We transpose because Julia saves each sample in a column
    data = np.array(data, dtype=np.float32)
    data_true = get_points_in_box(data, L)
    del data

    assert data_true.shape[0] >= sample_size

    K = data_true.shape[1]
    k = get_k(K, L)

    # Now we will use the data within the box, and replicate it around the box to build a shell:
    new_data = []
    data_for_gmm = np.copy(data_true[:sample_size])  # data in box to be sampled
    # For each dimension, i.e. each particle, create a shell on that dimension:
    for i in range(data_true.shape[1]):
        new_data_right = np.copy(data_for_gmm)
        new_data_left = np.copy(data_for_gmm)
        new_data_right[:, i] += L
        new_data_left[:, i] -= L
        data_for_gmm = np.vstack((data_for_gmm, new_data_right, new_data_left))

    if verbose:
        assert K == 2
        x = np.linspace(0, L, 100)
        y = np.linspace(0, L, 100)
        xx, yy = np.meshgrid(x, y)
        Z = np.empty((100, 100))
        for i in range(100):
            for j in range(100):
                p = np.abs(psi(np.array([x[i], y[j]]), k)) ** 2
                Z[i, j] = p
        plt.contourf(xx, yy, Z, levels=100)
        plt.axis("equal")
        plt.show()

    gmm = GaussianMixture(
        n_components=n_components, covariance_type="full", max_iter=5000, random_state=0
    )
    gmm.fit(data_for_gmm)

    factor = (
        1
        / nquad(
            gaussians_integrate,
            [[0, 1]] * K,
            args=(gmm.means_, gmm.covariances_, K, gmm.weights_,),
        )[0]
    )
    overlap = nquad(
        overlap_integrate,
        [[0, 1]] * data_for_gmm.shape[1],
        args=(factor, k, gmm.means_, gmm.covariances_, K, gmm.weights_,),
    )[0]

    if verbose:
        assert K == 2
        x = np.linspace(0, L, 100)
        y = np.linspace(0, L, 100)
        xx, yy = np.meshgrid(x, y)
        Z = np.empty((100, 100))
        for i in range(100):
            for j in range(100):
                p = gaussians(
                    np.array([x[i], y[j]]),
                    gmm.means_,
                    gmm.covariances_,
                    euclidean,
                    2,
                    gmm.weights_,
                )
                Z[i, j] = p
        plt.contourf(xx, yy, Z, levels=100)
        plt.axis("equal")
        plt.show()
    aic = gmm.aic(data_true)
    bic = gmm.bic(data_true)
    return overlap, gmm.weights_, gmm.means_, gmm.covariances_, aic, bic


def worker(triplet):
    num_particles, sample_size, n_components = triplet
    filename = f"particle_positions_N={num_particles}_metro_steps=10000000.npz"
    res = fit_GMM(
        filename, n_components, sample_size, L=1, verbose=False
    )
    return res


def test1(npart=2, ssize=int(1e5), ncomp=256):
    worker((npart, ssize, ncomp))


def test2():
    res = fit_GMM(
        "particle_positions_N=2_metro_steps=1000000.npz",
        n_components=10,
        sample_size=10000,
        L=1,
        verbose=True,
    )
    print(res[0])


def experiment1(n_cores=4):
    num_particle_range = [2, 3][::-1]
    sample_sizes = [int(1e4), int(1e5), int(1e6)][::-1]
    num_components = [4, 8, 16, 32, 64, 128, 256][::-1]

    iterable = list(itertools.product(num_particle_range, sample_sizes, num_components))
    if n_cores > 1:
        with Pool(n_cores) as p:
            r = list(tqdm.tqdm(p.imap(worker, iterable), total=len(iterable)))
    else:
        r = []
        for elem in tqdm.tqdm(iterable, total=len(iterable)):
            r.append(worker(elem))

    with open("all_results.p", "wb") as f:
        pickle.dump(r, f)


if __name__ == "__main__":
    #test2()
    experiment1(n_cores=1)
    # res = fit_GMM("particle_positions_N=2_metro_steps=10000000.npz", n_components=10, sample_size=100000, num_batches=50, L=1, verbose=False)
    # print(res[0])
    # print(np.mean(res[0]))
    # print(np.std(res[0]))
